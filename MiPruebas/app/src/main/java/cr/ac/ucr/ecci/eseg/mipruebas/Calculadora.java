package cr.ac.ucr.ecci.eseg.mipruebas;

public class Calculadora {
    public Calculadora() { }
    public int suma(int x, int y) {
        return x+y;
    }
    public int resta(int x, int y) {
        return x-y;
    }
    public int multiplica(int x, int y) {
        return x * y;
    }
}
