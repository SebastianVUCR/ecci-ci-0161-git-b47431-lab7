package cr.ac.ucr.ecci.eseg.mipruebas;
import android.content.Context;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import org.junit.Test;
import org.junit.runner.RunWith;
import static junit.framework.TestCase.assertEquals;

@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTestCxt {
    @Test
    public void testContext() throws Exception {
        // Context of the app under test.
        Context appContext = ApplicationProvider.getApplicationContext();
        assertEquals("cr.ac.ucr.ecci.eseg.mipruebas", appContext.getPackageName());
    }
}
