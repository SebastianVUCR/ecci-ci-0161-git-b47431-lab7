package cr.ac.ucr.ecci.eseg.mipruebas;
import android.content.Context;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static junit.framework.TestCase.assertEquals;

@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTestDb {
    public static final String TEST_STRING_ID = "1001";
    public static final String TEST_STRING_NAME = "Immanuel Kant";
    private Context mContext;
    private DataBaseDataSource mDataBaseDataSource;
    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule
            = new ActivityScenarioRule<>(MainActivity.class);
    @Test
    public void testDataBaseDataSourceLeerPersona() throws Exception {
        mContext = ApplicationProvider.getApplicationContext();
        mDataBaseDataSource = new DataBaseDataSource(mContext);
        Persona mPersona = mDataBaseDataSource.leerPersona(TEST_STRING_ID);
        assertEquals(TEST_STRING_ID, mPersona.getIdentificacion());
        assertEquals(TEST_STRING_NAME, mPersona.getNombre());
    }
}